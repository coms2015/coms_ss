package coms.server.dao;

import coms.server.entity.EGoods;
import coms.server.entity.EStock;
import coms.server.entity.EStore;

import java.util.List;

public interface StockDAO {
    void addStock(EStock stock);

    void removeStock(int n);

    void editStock(EStock stock);

    @SuppressWarnings("unchecked")
    List<EStock> stock(EStore store, boolean activeStock);

    @SuppressWarnings("unchecked")
    List<EStock> stockByCriteria(EStore store, EGoods goods, int amount);
}
