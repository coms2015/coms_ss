package coms.server.dao;

import coms.server.entity.EStore;

import java.util.List;

public interface StoreDAO {
    List<EStore> stores();
    void addStore(EStore store);
    void removeStore(Integer n);
    void editStore(EStore store);
}
