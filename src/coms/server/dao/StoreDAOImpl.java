package coms.server.dao;

import coms.server.entity.EStore;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class StoreDAOImpl implements StoreDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<EStore> stores() {
        return sessionFactory.getCurrentSession().createQuery("from EStore").list();
    }

    @Override
    public void addStore(EStore store) {
        sessionFactory.getCurrentSession().save(store);
    }

    @Override
    public void removeStore(Integer n) {
        EStore store = (EStore) sessionFactory.getCurrentSession().load(EStore.class, n);
        if (store != null) {
            sessionFactory.getCurrentSession().delete(store);
        }
    }

    @Override
    public void editStore(EStore store) {
        sessionFactory.getCurrentSession().update(store);
    }
}
