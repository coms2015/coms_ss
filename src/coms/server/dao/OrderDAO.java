package coms.server.dao;

import coms.server.entity.EOrder;
import coms.server.entity.EOrdersGoods;

import java.util.List;

public interface OrderDAO {
    void addOrder(EOrder order);

    void removeOrder(int n);

    @SuppressWarnings("unchecked")
    List<EOrder> orders();

    @SuppressWarnings("unchecked")
    List<EOrder> ordersByUserN(int n);

    void editOrder(EOrder order);

    void saveOrUpdateGoods(EOrdersGoods goods);

    EOrder getOrderByN(int orderN);
}
