package coms.server.dao;

import coms.server.entity.EOrder;
import coms.server.entity.EReceipt;

import java.util.List;

public interface ReceiptDAO {

    void addReceipt(EReceipt receipt);

    void removeReceipt(int n);

    @SuppressWarnings("unchecked")
    List<EReceipt> receipts();

    void editReceipt(EReceipt receipt);

    EReceipt getReceiptByN(int n);
}
