package coms.server.dao;

import coms.server.entity.EUser;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(EUser user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public void removeUser(EUser user) {
        sessionFactory.getCurrentSession().delete(user);
    }

    @SuppressWarnings("unchecked")
    @Override
    public EUser authUser(String login, String hash) {
        List<EUser> userList = sessionFactory.getCurrentSession().createQuery("from EUser where login = :login").setParameter("login", login.toLowerCase()).list();
        if (userList.size() > 0) {
            EUser user = userList.get(0);
            if (user.getPassword().equalsIgnoreCase(hash))
                return user;
            else
                return null;
        }
        return null;
    }
}
