package coms.server.dao;

import coms.server.entity.EGoods;
import coms.server.entity.EGoodsBarcodes;

public interface GoodsBarcodeDAO {
    void addBarcode(EGoodsBarcodes goodsBarcodes);

    void removeBarcode(int n);

    void removeAllBarcodes(EGoods goods);
}
