package coms.server.dao;

import coms.server.entity.EGoods;
import coms.server.entity.EGoodsBarcodes;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class GoodsBarcodeDAOImpl implements GoodsBarcodeDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addBarcode(EGoodsBarcodes goodsBarcodes) {
        try {
            sessionFactory.getCurrentSession().save(goodsBarcodes);
        } catch (NonUniqueObjectException ignored) {}
    }

    @Override
    public void removeBarcode(int n) {
        EGoodsBarcodes goodsBarcodes = (EGoodsBarcodes) sessionFactory.getCurrentSession().load(EGoodsBarcodes.class, n);
        sessionFactory.getCurrentSession().delete(goodsBarcodes);
    }

    @Override
    public void removeAllBarcodes(EGoods goods) {
        sessionFactory.getCurrentSession().createQuery("delete EGoodsBarcodes where goodsByGoodsN = :goods").setParameter("goods", goods).executeUpdate();
    }
}
