package coms.server.dao;

import coms.server.entity.EGoods;
import coms.server.entity.EGoodsStores;
import coms.server.entity.EStore;

public interface GoodsStoreDAO {
    void addLink(EGoodsStores link);

    void removeLink(EGoods goods, EStore store);

    void removeAllLink(EGoods goods);
}
