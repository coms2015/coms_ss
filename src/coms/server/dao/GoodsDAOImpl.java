package coms.server.dao;

import coms.server.entity.EGoods;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class GoodsDAOImpl implements GoodsDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addGoods(EGoods goods) {
        sessionFactory.getCurrentSession().save(goods);
    }

    @Override
    public void removeGoods(int n) {
        EGoods goods = (EGoods) sessionFactory.getCurrentSession().load(EGoods.class, n);
        if (goods != null) {
            sessionFactory.getCurrentSession().delete(goods);
        }
    }

    @Override
    public void editGoods(EGoods goods) {
        sessionFactory.getCurrentSession().update(goods);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<EGoods> goods() {
        return sessionFactory.getCurrentSession().createQuery("from EGoods").list();
    }

    @Override
    public EGoods getGoodsByN(int n) {
        return (EGoods) sessionFactory.getCurrentSession().load(EGoods.class, n);
    }
}
