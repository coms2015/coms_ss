package coms.server.dao;

import coms.server.entity.EGoods;
import coms.server.entity.EGoodsStores;
import coms.server.entity.EStore;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class GoodsStoreDAOImpl implements GoodsStoreDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addLink(EGoodsStores link) {
        try {
            sessionFactory.getCurrentSession().save(link);
        } catch (NonUniqueObjectException ignored) {}
    }

    @Override
    public void removeLink(EGoods goods, EStore store) {
        sessionFactory.getCurrentSession().createQuery("delete from EGoodsStores where goodsByGoodsN = :goods and storesByStoresN = :store")
                .setParameter("goods", goods).setParameter("store", store).executeUpdate();
    }

    @Override
    public void removeAllLink(EGoods goods) {
        sessionFactory.getCurrentSession().createQuery("delete from EGoodsStores where goodsByGoodsN = :goods").setParameter("goods", goods).executeUpdate();
    }
}
