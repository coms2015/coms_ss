package coms.server.dao;

import coms.server.entity.EGoods;

import java.util.List;

public interface GoodsDAO {
    void addGoods(EGoods goods);

    void removeGoods(int n);

    void editGoods(EGoods goods);

    @SuppressWarnings("unchecked")
    List<EGoods> goods();

    EGoods getGoodsByN(int n);
}
