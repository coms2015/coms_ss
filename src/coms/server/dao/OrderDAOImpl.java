package coms.server.dao;

import coms.server.entity.EOrder;
import coms.server.entity.EOrdersGoods;
import coms.server.entity.EUser;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class OrderDAOImpl implements OrderDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addOrder(EOrder order) {
        sessionFactory.getCurrentSession().save(order);
        order.getOrdersGoodsesByN().forEach(g -> sessionFactory.getCurrentSession().save(g));
    }

    @Override
    public void removeOrder(int n) {
        EOrder order = (EOrder) sessionFactory.getCurrentSession().load(EOrder.class, n);
        if (order != null) {
            sessionFactory.getCurrentSession().delete(order);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EOrder> orders() {
        return sessionFactory.getCurrentSession().createQuery("from EOrder").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EOrder> ordersByUserN(int n) {
        EUser user = (EUser) sessionFactory.getCurrentSession().load(EUser.class, n);
        return sessionFactory.getCurrentSession().createQuery("from EOrder where usersByUsersN = :user").setParameter("user", user).list();
    }

    @Override
    public void editOrder(EOrder order) {
        sessionFactory.getCurrentSession().update(order);
//        sessionFactory.getCurrentSession().createQuery("delete from EOrdersGoods where ordersByOrdersN = :order").setParameter("order", order).executeUpdate();
//        for (EOrdersGoods goods : order.getOrdersGoodsesByN()) {
//            sessionFactory.getCurrentSession().save(goods);
//        }
    }

    @Override
    public void saveOrUpdateGoods(EOrdersGoods goods) {
        sessionFactory.getCurrentSession().saveOrUpdate(goods);
    }

    @Override
    public EOrder getOrderByN(int orderN) {
        return (EOrder) sessionFactory.getCurrentSession().load(EOrder.class, orderN);
    }
}
