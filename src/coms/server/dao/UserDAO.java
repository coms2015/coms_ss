package coms.server.dao;

import coms.server.entity.EUser;

public interface UserDAO {
    void addUser(EUser user);
    void removeUser(EUser user);
    EUser authUser(String login, String hash);
}
