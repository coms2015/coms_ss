package coms.server.dao;

import coms.server.entity.EReceipt;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class ReceiptDAOImpl implements ReceiptDAO {
    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void addReceipt(EReceipt receipt) {
        sessionFactory.getCurrentSession().save(receipt);
    }

    @Override
    public void removeReceipt(int n) {
        EReceipt receipt = (EReceipt) sessionFactory.getCurrentSession().load(EReceipt.class, n);
        if (receipt != null) {
            sessionFactory.getCurrentSession().delete(receipt);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EReceipt> receipts() {
        return sessionFactory.getCurrentSession().createQuery("from EReceipt").list();
    }

    @Override
    public void editReceipt(EReceipt receipt) {
        sessionFactory.getCurrentSession().update(receipt);
        sessionFactory.getCurrentSession().createQuery("delete from EReceiptsGoods where :receipt = receiptsByReceiptsN")
            .setParameter("receipt", receipt).executeUpdate();
        receipt.getReceiptsGoodsesByN().forEach(r -> sessionFactory.getCurrentSession().save(r));
    }

    @Override
    public EReceipt getReceiptByN(int n) {
        return (EReceipt) sessionFactory.getCurrentSession().load(EReceipt.class, n);
    }
}
