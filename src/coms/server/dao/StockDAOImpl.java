package coms.server.dao;

import coms.server.entity.EGoods;
import coms.server.entity.EStock;
import coms.server.entity.EStore;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class StockDAOImpl implements StockDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public void addStock(EStock stock) {
        List<EStock> currentStock = sessionFactory.getCurrentSession().createQuery("from EStock where storesByStoresN = :store and goodsByGoodsN = :goods and inOrder = 0")
                .setParameter("store", stock.getStoresByStoresN()).setParameter("goods", stock.getGoodsByGoodsN()).list();
        if (currentStock.size() > 0) {
            EStock addStock = currentStock.get(0);
            addStock.setAmount(addStock.getAmount() + stock.getAmount());
            sessionFactory.getCurrentSession().update(addStock);
        } else {
            sessionFactory.getCurrentSession().save(stock);
        }
    }

    @Override
    public void removeStock(int n) {
        EStock stock = (EStock) sessionFactory.getCurrentSession().load(EStock.class, n);
        if (stock != null) {
            sessionFactory.getCurrentSession().delete(stock);
        }
    }

    @Override
    public void editStock(EStock stock) {
        sessionFactory.getCurrentSession().update(stock);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EStock> stock(EStore store, boolean activeStock) {
        if (store == null && !activeStock)
            return sessionFactory.getCurrentSession().createQuery("from EStock").list();
        if (store == null)
            return sessionFactory.getCurrentSession().createQuery("from EStock where inOrder = 0").list();
        if (!activeStock)
            return sessionFactory.getCurrentSession().createQuery("from EStock where EStore = :store").setParameter("store", store).list();
        return sessionFactory.getCurrentSession().createQuery("from EStock where EStore = :store and inOrder = 0").setParameter("store", store).list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EStock> stockByCriteria(EStore store, EGoods goods, int amount) {
        return sessionFactory.getCurrentSession().createQuery("from EStock where storesByStoresN = :store and goodsByGoodsN = :goods and amount >= :amount and inOrder = 0")
                .setParameter("store", store).setParameter("goods", goods).setParameter("amount", amount).list();
    }
}
