package coms.server.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "goods", schema = "", catalog = "coms")
public class EGoods {
    private Integer n;
    private String code;
    private String name;
    private Collection<EGoodsBarcodes> goodsBarcodesByN;
    private Collection<EGoodsStores> goodsStoresByN;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "n", nullable = false, insertable = true, updatable = true)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Basic
    @Column(name = "code", nullable = false, insertable = true, updatable = true, length = 50)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EGoods eGoods = (EGoods) o;

        return !(n != null ? !n.equals(eGoods.n) : eGoods.n != null) && !(code != null ? !code.equals(eGoods.code) : eGoods.code != null) && !(name != null ? !name.equals(eGoods.name) : eGoods.name != null);

    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "goodsByGoodsN", fetch = FetchType.LAZY)  //EAGER
    public Collection<EGoodsBarcodes> getGoodsBarcodesByN() {
        return goodsBarcodesByN;
    }

    public void setGoodsBarcodesByN(Collection<EGoodsBarcodes> goodsBarcodesByN) {
        this.goodsBarcodesByN = goodsBarcodesByN;
    }

    @OneToMany(mappedBy = "goodsByGoodsN", fetch = FetchType.LAZY)
    public Collection<EGoodsStores> getGoodsStoresByN() {
        return goodsStoresByN;
    }

    public void setGoodsStoresByN(Collection<EGoodsStores> goodsStoresByN) {
        this.goodsStoresByN = goodsStoresByN;
    }
}
