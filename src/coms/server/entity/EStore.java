package coms.server.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "stores", schema = "", catalog = "coms")
public class EStore {
    private int n;
    private String name;
    private String address;
//    private Collection<EGoodsStores> goodsStoresByN;
//    private Collection<EOrder> ordersesByN;
//    private Collection<EReceipt> receiptsesByN;
//    private Collection<EStock> stocksByN;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "n", nullable = false, insertable = true, updatable = true)
    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "address", nullable = false, insertable = true, updatable = true, length = 500)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EStore eStore = (EStore) o;

        return n == eStore.n && !(name != null ? !name.equals(eStore.name) : eStore.name != null) && !(address != null ? !address.equals(eStore.address) : eStore.address != null);

    }

    @Override
    public int hashCode() {
        int result = n;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

//    @JsonIgnore
//    @OneToMany(mappedBy = "storesByStoresN", fetch = FetchType.LAZY)
//    public Collection<EGoodsStores> getGoodsStoresByN() {
//        return goodsStoresByN;
//    }
//
//    public void setGoodsStoresByN(Collection<EGoodsStores> goodsStoresByN) {
//        this.goodsStoresByN = goodsStoresByN;
//    }

//    @OneToMany(mappedBy = "storesByStoresN")
//    public Collection<EOrder> getOrdersesByN() {
//        return ordersesByN;
//    }
//
//    public void setOrdersesByN(Collection<EOrder> ordersesByN) {
//        this.ordersesByN = ordersesByN;
//    }
//
//    @OneToMany(mappedBy = "storesByStoresN")
//    public Collection<EReceipt> getReceiptsesByN() {
//        return receiptsesByN;
//    }
//
//    public void setReceiptsesByN(Collection<EReceipt> receiptsesByN) {
//        this.receiptsesByN = receiptsesByN;
//    }
//
//    @OneToMany(mappedBy = "storesByStoresN")
//    public Collection<EStock> getStocksByN() {
//        return stocksByN;
//    }
//
//    public void setStocksByN(Collection<EStock> stocksByN) {
//        this.stocksByN = stocksByN;
//    }
}
