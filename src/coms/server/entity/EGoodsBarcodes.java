package coms.server.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "goods_barcodes", schema = "", catalog = "coms")
public class EGoodsBarcodes {
    private String barcode;
    private Integer n;
    private EGoods goodsByGoodsN;

    @Basic
    @Column(name = "barcode", nullable = false, insertable = true, updatable = true, length = 45)
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "n", nullable = false, insertable = true, updatable = true)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EGoodsBarcodes that = (EGoodsBarcodes) o;

        if (barcode != null ? !barcode.equals(that.barcode) : that.barcode != null) return false;
        return !(n != null ? !n.equals(that.n) : that.n != null);

    }

    @Override
    public int hashCode() {
        int result = barcode != null ? barcode.hashCode() : 0;
        result = 31 * result + (n != null ? n.hashCode() : 0);
        return result;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "goods_n", referencedColumnName = "n", nullable = false)
    public EGoods getGoodsByGoodsN() {
        return goodsByGoodsN;
    }

    public void setGoodsByGoodsN(EGoods goodsByGoodsN) {
        this.goodsByGoodsN = goodsByGoodsN;
    }
}
