package coms.server.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "roles", schema = "", catalog = "coms")
public class ERole {
    private Integer n;
    private String name;
    private String modules;
    private Collection<EUser> usersesByN;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "n", nullable = false, insertable = true, updatable = true)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "modules", nullable = false, insertable = true, updatable = true, length = 500)
    public String getModules() {
        return modules;
    }

    public void setModules(String modules) {
        this.modules = modules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ERole role = (ERole) o;

        return !(n != null ? !n.equals(role.n) : role.n != null) && !(name != null ? !name.equals(role.name) : role.name != null) && !(modules != null ? !modules.equals(role.modules) : role.modules != null);

    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (modules != null ? modules.hashCode() : 0);
        return result;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "rolesByRolesN", fetch = FetchType.LAZY)
    public Collection<EUser> getUsersesByN() {
        return usersesByN;
    }

    public void setUsersesByN(Collection<EUser> usersesByN) {
        this.usersesByN = usersesByN;
    }
}
