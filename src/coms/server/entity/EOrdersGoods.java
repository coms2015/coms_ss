package coms.server.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "orders_goods", schema = "", catalog = "coms")
public class EOrdersGoods {
    private Integer amount;
    private Integer n;
    private EGoods goodsByGoodsN;
    private EOrder ordersByOrdersN;
    private EStock stockByStockN;

    @Basic
    @Column(name = "amount", nullable = false, insertable = true, updatable = true)
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "n", nullable = false, insertable = true, updatable = true)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EOrdersGoods that = (EOrdersGoods) o;

        return !(amount != null ? !amount.equals(that.amount) : that.amount != null) && !(n != null ? !n.equals(that.n) : that.n != null);

    }

    @Override
    public int hashCode() {
        int result = amount != null ? amount.hashCode() : 0;
        result = 31 * result + (n != null ? n.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "goods_n", referencedColumnName = "n", nullable = false)
    public EGoods getGoodsByGoodsN() {
        return goodsByGoodsN;
    }

    public void setGoodsByGoodsN(EGoods goodsByGoodsN) {
        this.goodsByGoodsN = goodsByGoodsN;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "orders_n", referencedColumnName = "n", nullable = false)
    public EOrder getOrdersByOrdersN() {
        return ordersByOrdersN;
    }

    public void setOrdersByOrdersN(EOrder ordersByOrdersN) {
        this.ordersByOrdersN = ordersByOrdersN;
    }

    @ManyToOne
    @JoinColumn(name = "stock_n", referencedColumnName = "n", nullable = true)
    public EStock getStockByStockN() {
        return stockByStockN;
    }

    public void setStockByStockN(EStock stockByStockN) {
        this.stockByStockN = stockByStockN;
    }
}
