package coms.server.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "users", schema = "", catalog = "coms")
public class EUser implements Serializable {
    private Integer n;
    private String login;
    private String mail;
    private String password;
    private Integer rolesN;
    private ERole rolesByRolesN;
    private Collection<EOrder> ordersesByN;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "n", nullable = false, insertable = true, updatable = true)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Basic
    @Column(name = "login", nullable = false, insertable = true, updatable = true, length = 45)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "mail", nullable = true, insertable = true, updatable = true, length = 100)
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Basic
    @Column(name = "password", nullable = false, insertable = true, updatable = true, length = 40)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "roles_n", nullable = false, insertable = false, updatable = false)
    public Integer getRolesN() {
        return rolesN;
    }

    public void setRolesN(Integer rolesN) {
        this.rolesN = rolesN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EUser user = (EUser) o;

        return !(n != null ? !n.equals(user.n) : user.n != null) && !(login != null ? !login.equals(user.login) : user.login != null) && !(mail != null ? !mail.equals(user.mail) : user.mail != null) && !(password != null ? !password.equals(user.password) : user.password != null) && !(rolesN != null ? !rolesN.equals(user.rolesN) : user.rolesN != null);

    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (mail != null ? mail.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (rolesN != null ? rolesN.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "roles_n", referencedColumnName = "n", nullable = false)
    public ERole getRolesByRolesN() {
        return rolesByRolesN;
    }

    public void setRolesByRolesN(ERole rolesByRolesN) {
        this.rolesByRolesN = rolesByRolesN;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "usersByUsersN", fetch = FetchType.LAZY)
    public Collection<EOrder> getOrdersesByN() {
        return ordersesByN;
    }

    public void setOrdersesByN(Collection<EOrder> ordersesByN) {
        this.ordersesByN = ordersesByN;
    }
}
