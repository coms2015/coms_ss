package coms.server.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "orders", schema = "", catalog = "coms")
public class EOrder {
    private Integer n;
    private String code;
    private Timestamp createdAt;
    private Date executeAt;
    private Integer executed;
    private EStore storesByStoresN;
    private EUser usersByUsersN;
    private Collection<EOrdersGoods> ordersGoodsesByN;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "n", nullable = false, insertable = true, updatable = true)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Basic
    @Column(name = "code", nullable = false, insertable = true, updatable = true, length = 45)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "created_at", nullable = false, insertable = true, updatable = true)
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "execute_at", nullable = true, insertable = true, updatable = true)
    public Date getExecuteAt() {
        return executeAt;
    }

    public void setExecuteAt(Date executeAt) {
        this.executeAt = executeAt;
    }

    @Basic
    @Column(name = "executed", nullable = false, insertable = true, updatable = true)
    public Integer getExecuted() {
        return executed;
    }

    public void setExecuted(Integer executed) {
        this.executed = executed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EOrder eOrder = (EOrder) o;

        return !(n != null ? !n.equals(eOrder.n) : eOrder.n != null) && !(code != null ? !code.equals(eOrder.code) : eOrder.code != null) && !(createdAt != null ? !createdAt.equals(eOrder.createdAt) : eOrder.createdAt != null) && !(executeAt != null ? !executeAt.equals(eOrder.executeAt) : eOrder.executeAt != null) && !(executed != null ? !executed.equals(eOrder.executed) : eOrder.executed != null);
    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (executeAt != null ? executeAt.hashCode() : 0);
        result = 31 * result + (executed != null ? executed.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "stores_n", referencedColumnName = "n", nullable = false)
    public EStore getStoresByStoresN() {
        return storesByStoresN;
    }

    public void setStoresByStoresN(EStore storesByStoresN) {
        this.storesByStoresN = storesByStoresN;
    }

    @ManyToOne
    @JoinColumn(name = "users_n", referencedColumnName = "n", nullable = false)
    public EUser getUsersByUsersN() {
        return usersByUsersN;
    }

    public void setUsersByUsersN(EUser usersByUsersN) {
        this.usersByUsersN = usersByUsersN;
    }

    @OneToMany(mappedBy = "ordersByOrdersN", fetch = FetchType.EAGER)
    public Collection<EOrdersGoods> getOrdersGoodsesByN() {
        return ordersGoodsesByN;
    }

    public void setOrdersGoodsesByN(Collection<EOrdersGoods> ordersGoodsesByN) {
        this.ordersGoodsesByN = ordersGoodsesByN;
    }
}
