package coms.server.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "receipts", schema = "", catalog = "coms")
public class EReceipt {
    private Integer n;
    private String code;
    private Timestamp createdAt;
    private Date arrivalAt;
    private Integer arrived;
    private EStore storesByStoresN;
    private Collection<EReceiptsGoods> receiptsGoodsesByN;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "n", nullable = false, insertable = true, updatable = true)
    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    @Basic
    @Column(name = "code", nullable = false, insertable = true, updatable = true, length = 45)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "created_at", nullable = false, insertable = true, updatable = true)
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "arrival_at", nullable = true, insertable = true, updatable = true)
    public Date getArrivalAt() {
        return arrivalAt;
    }

    public void setArrivalAt(Date arrivalAt) {
        this.arrivalAt = arrivalAt;
    }

    @Basic
    @Column(name = "arrived", nullable = false, insertable = true, updatable = true)
    public Integer getArrived() {
        return arrived;
    }

    public void setArrived(Integer arrived) {
        this.arrived = arrived;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EReceipt eReceipt = (EReceipt) o;

        if (n != null ? !n.equals(eReceipt.n) : eReceipt.n != null) return false;
        if (code != null ? !code.equals(eReceipt.code) : eReceipt.code != null) return false;
        if (createdAt != null ? !createdAt.equals(eReceipt.createdAt) : eReceipt.createdAt != null) return false;
        if (arrivalAt != null ? !arrivalAt.equals(eReceipt.arrivalAt) : eReceipt.arrivalAt != null) return false;
        return !(arrived != null ? !arrived.equals(eReceipt.arrived) : eReceipt.arrived != null);

    }

    @Override
    public int hashCode() {
        int result = n != null ? n.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (arrivalAt != null ? arrivalAt.hashCode() : 0);
        result = 31 * result + (arrived != null ? arrived.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "stores_n", referencedColumnName = "n", nullable = false)
    public EStore getStoresByStoresN() {
        return storesByStoresN;
    }

    public void setStoresByStoresN(EStore storesByStoresN) {
        this.storesByStoresN = storesByStoresN;
    }

    @OneToMany(mappedBy = "receiptsByReceiptsN", fetch = FetchType.EAGER)
    public Collection<EReceiptsGoods> getReceiptsGoodsesByN() {
        return receiptsGoodsesByN;
    }

    public void setReceiptsGoodsesByN(Collection<EReceiptsGoods> receiptsGoodsesByN) {
        this.receiptsGoodsesByN = receiptsGoodsesByN;
    }
}
