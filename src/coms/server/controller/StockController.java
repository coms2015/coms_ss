package coms.server.controller;

import coms.exchange.JStockMove;
import coms.exchange.JStockMoveResponse;
import coms.server.entity.EStock;
import coms.server.service.MoveService;
import coms.server.service.StockService;
import coms.server.service.UserSession;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.List;

@Controller
public class StockController {
    @Autowired
    private StockService stockService;

    @Autowired
    private MoveService moveService;

    @Autowired
    private UserSession session;

    private ObjectMapper mapper = new ObjectMapper();

    //TODO вопрос по больщому стоку - делить на отдельыне json
    @RequestMapping(value = "/stock/list", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getStockList() {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("stock"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        List<EStock> stock = stockService.getStock();
        try {
            String response = mapper.writeValueAsString(stock);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/stock/move", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> moveStock(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("move"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JStockMove stockMove = mapper.readValue(request, JStockMove.class);
            moveService.moveStock(stockMove.getStock(), stockMove.getStore());
            JStockMoveResponse response = new JStockMoveResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
