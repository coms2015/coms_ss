package coms.server.controller;

import coms.exchange.*;
import coms.server.entity.EStore;
import coms.server.service.StoreService;
import coms.server.service.UserSession;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.List;

@Controller
public class StoreController {
    @Autowired
    private StoreService storeService;

    @Autowired
    private UserSession session;

    private ObjectMapper mapper = new ObjectMapper();

    @RequestMapping(value = "/store/list", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getStoresList() {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        if (!session.checkModuleAccess("stores"))
//            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        List<EStore> stores = storeService.stores();
        try {
            String response = mapper.writeValueAsString(stores);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/store/add", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> addStore(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("stores"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JStoreAdd storeAdd = mapper.readValue(request, JStoreAdd.class);
            storeService.addStore(storeAdd.getStore());
            JStoreAddResponse response = new JStoreAddResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/store/remove", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> removeStore(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("stores"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JStoreRemove storeRemove = mapper.readValue(request, JStoreRemove.class);
            storeService.removeStore(storeRemove.getStoreN());
            JStoreRemoveResponse response = new JStoreRemoveResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/store/edit", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> editStore(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("stores"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JStoreEdit storeEdit = mapper.readValue(request, JStoreEdit.class);
            storeService.editStore(storeEdit.getStore());
            JStoreEditResponse response = new JStoreEditResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
