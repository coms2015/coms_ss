package coms.server.controller;

import coms.exchange.*;
import coms.server.entity.EReceipt;
import coms.server.service.ReceiptService;
import coms.server.service.UserSession;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.List;

@Controller
public class ReceiptController {
    @Autowired
    private ReceiptService receiptService;

    @Autowired
    private UserSession session;

    private ObjectMapper mapper = new ObjectMapper();

    @RequestMapping(value = "/receipt/list", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getReceipts() {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("receipts"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        List<EReceipt> receipts = receiptService.receipts();
        try {
            String response = mapper.writeValueAsString(receipts);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/receipt/add", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> addReceipt(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("receipts"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JReceiptAdd receiptAdd = mapper.readValue(request, JReceiptAdd.class);
            receiptService.addReceipt(receiptAdd.getReceipt());
            JReceiptAddResponse response = new JReceiptAddResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/receipt/remove", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> removeReceipt(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("receipts"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JReceiptRemove receiptRemove = mapper.readValue(request, JReceiptRemove.class);
            receiptService.removeReceipt(receiptRemove.getReceipt().getN());
            JReceiptRemoveResponse response = new JReceiptRemoveResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/receipt/edit", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> editReceipt(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("receipts"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JReceiptEdit receiptEdit = mapper.readValue(request, JReceiptEdit.class);
            receiptService.editReceipt(receiptEdit.getReceipt());
            JReceiptEditResponse response = new JReceiptEditResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/receipt/in", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> receiptToStock(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("receipts"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JReceiptIn receiptIn = mapper.readValue(request, JReceiptIn.class);
            receiptService.putReceiptToStock(receiptIn.getReceipt().getN());
            JReceiptInResponse response = new JReceiptInResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
