package coms.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class InfoController {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public View redirect() {
        return new RedirectView("https://bitbucket.org/sedmess/coms_ss");
    }
}
