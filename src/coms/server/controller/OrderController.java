package coms.server.controller;

import coms.exchange.*;
import coms.server.EditDenyException;
import coms.server.ReserveDeficiencyException;
import coms.server.service.OrderService;
import coms.server.service.UserSession;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;

@Controller
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private UserSession session;

    private ObjectMapper mapper = new ObjectMapper();

    public OrderController() {
        mapper.configure(org.codehaus.jackson.map.SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
    }

    @RequestMapping(value = "/order/list", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getOrders() {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("orders"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            String response = orderService.orderAsString(mapper);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/order/listbyuser", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getOrdersByUser() {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("user_orders"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            String response = orderService.ordersByUserNAsString(session.getUser().getN(), mapper);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/order/add", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> addOrder(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("user_orders") && !session.checkModuleAccess("orders"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JOrderAdd orderAdd = mapper.readValue(request, JOrderAdd.class);
            //access level check start
//            if (!session.checkModuleAccess("orders") && !session.getUser().equals(orderAdd.getOrder().getUsersByUsersN())) {
//                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
//            }
            //access level check end
            orderService.addOrder(orderAdd.getOrder());
            JOrderAddResponse response = new JOrderAddResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/order/remove", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> removeOrder(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("user_orders") && !session.checkModuleAccess("orders"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JOrderRemove orderRemove = mapper.readValue(request, JOrderRemove.class);
            //access level check start
//            if (!session.checkModuleAccess("orders") && !session.getUser().equals(orderRemove.getOrder().getUsersByUsersN())) {
//                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
//            }
            //access level check end
            orderService.removeOrder(orderRemove.getOrder().getN());
            JOrderRemoveResponse response = new JOrderRemoveResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/order/edit", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> editOrder(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("user_orders") && !session.checkModuleAccess("orders"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JOrderEdit orderEdit = mapper.readValue(request, JOrderEdit.class);
            //access level check start
//            if (!session.checkModuleAccess("orders") && !session.getUser().equals(orderEdit.getOrder().getUsersByUsersN())) {
//                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
//            }
            //access level check end
            try {
                orderService.editOrder(orderEdit.getOrder());
            } catch (EditDenyException e) {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
            JOrderEditResponse response = new JOrderEditResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/order/reserve", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> reserveOrder(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("orders"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JOrderReserve orderReserve = mapper.readValue(request, JOrderReserve.class);
            try {
                orderService.reserveStock(orderReserve.getOrder().getN());
            } catch (ReserveDeficiencyException e) {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.INSUFFICIENT_STORAGE);
            }
            JOrderReserveResponse response = new JOrderReserveResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/order/out", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> outOrder(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("orders"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JOrderOut orderOut = mapper.readValue(request, JOrderOut.class);
            orderService.getGoodsFromStock(orderOut.getOrder().getN());
            JOrderOutResponse response = new JOrderOutResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
