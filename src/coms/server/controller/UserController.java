package coms.server.controller;

import coms.exchange.JAuth;
import coms.exchange.JAuthResponse;
import coms.exchange.JLogout;
import coms.exchange.JLogoutResponse;
import coms.server.service.UserSession;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;

@Controller
public class UserController {
    @Autowired
    private UserSession session;

    private ObjectMapper mapper = new ObjectMapper();

    @RequestMapping(value = "/auth", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> tryToAuth(@RequestBody String request) {
        try {
            JAuth auth = mapper.readValue(request, JAuth.class);
            if (session.tryToAuth(auth.getLogin(), auth.getHash())) {
                String[] modules = (String[]) session.getModules().toArray();
                JAuthResponse response = new JAuthResponse();
                response.setModules(modules);
                return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "logout", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> logout(@RequestBody String request) {
        try {
            mapper.readValue(request, JLogout.class);
            session.logOut();
            JLogoutResponse response = new JLogoutResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
