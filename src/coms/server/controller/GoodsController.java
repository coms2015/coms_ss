package coms.server.controller;

import coms.exchange.*;
import coms.server.entity.EGoods;
import coms.server.entity.EStore;
import coms.server.service.GoodsService;
import coms.server.service.UserSession;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

@Controller
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @Autowired
    private UserSession session;

    private ObjectMapper mapper = new ObjectMapper();

    @RequestMapping(value = "/goods/list", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getGoodsList() {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        if (!session.checkModuleAccess("goods"))
//            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        List<EGoods> goods = goodsService.goods();
        try {
            String response = mapper.writeValueAsString(goods);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/goods/stores", method = RequestMethod.GET, produces = "application/json;charset=UTF-8", params = {"n"})
    public ResponseEntity<String> getGoodsStoresList(@RequestParam(value = "n") int n) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        if (!session.checkModuleAccess("goods"))
//            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        List<EStore> stores = goodsService.getStoresByGoods(n);
        try {
            String response = mapper.writeValueAsString(stores);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            //TODO логгер
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/goods/add", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> addGoods(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("goods"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JGoodsAdd goodsAdd = mapper.readValue(request, JGoodsAdd.class);
            goodsService.addGoods(goodsAdd.getGoods());
            JGoodsAddResponse response = new JGoodsAddResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/goods/edit", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> editGoods(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("goods"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JGoodsEdit goodsEdit = mapper.readValue(request, JGoodsEdit.class);
            goodsService.editGoods(goodsEdit.getGoods());
            JGoodsEditResponse response = new JGoodsEditResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/goods/remove", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> removeGoods(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("goods"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JGoodsRemove goodsRemove = mapper.readValue(request, JGoodsRemove.class);
            goodsService.removeGoods(goodsRemove.getGoodsN());
            JGoodsRemoveResponse response = new JGoodsRemoveResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/goods/addlink", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> addGoodsStoreLink(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("goods"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JGoodsStoreLinkAdd goodsStoreLinkAdd = mapper.readValue(request, JGoodsStoreLinkAdd.class);
            goodsService.addStore(goodsStoreLinkAdd.getGoods(), goodsStoreLinkAdd.getStore());
            JGoodsStoreLinkAddResponse response = new JGoodsStoreLinkAddResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/goods/removelinks", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> removeGoodsStoreLink(@RequestBody String request) {
        /////////////////// security start
        if (!session.isLoggedIn())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if (!session.checkModuleAccess("goods"))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        ////////////////// security end
        try {
            JGoodsStoreLinkRemove goodsStoreLinkRemove = mapper.readValue(request, JGoodsStoreLinkRemove.class);
            goodsService.removeStore(goodsStoreLinkRemove.getGoods(), goodsStoreLinkRemove.getStore());
            JGoodsStoreLinkRemoveResponse response = new JGoodsStoreLinkRemoveResponse();
            return new ResponseEntity<>(mapper.writeValueAsString(response), HttpStatus.OK);
        } catch (JsonMappingException | JsonParseException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
