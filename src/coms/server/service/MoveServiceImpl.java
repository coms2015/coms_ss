package coms.server.service;

import coms.server.dao.StockDAO;
import coms.server.entity.EStock;
import coms.server.entity.EStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MoveServiceImpl implements MoveService {
    @Autowired
    private StockDAO stockDAO;

    @Override
    public void moveStock(List<EStock> stock, EStore toStore) {
        for (EStock stockEntry : stock) {
            stockEntry.setStoresByStoresN(toStore);
            stockDAO.editStock(stockEntry);
        }
    }
}
