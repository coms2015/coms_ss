package coms.server.service;


import coms.server.dao.GoodsBarcodeDAO;
import coms.server.dao.GoodsDAO;
import coms.server.dao.GoodsStoreDAO;
import coms.server.entity.EGoods;
import coms.server.entity.EGoodsStores;
import coms.server.entity.EStore;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private GoodsDAO goodsDAO;
    @Autowired
    private GoodsStoreDAO goodsStoreDAO;
    @Autowired
    private GoodsBarcodeDAO goodsBarcodeDAO;

    private void recoverLinkages(EGoods goods) {
        goods.getGoodsBarcodesByN().forEach(s -> s.setGoodsByGoodsN(goods));
        goods.getGoodsStoresByN().forEach(s -> s.setGoodsByGoodsN(goods));
    }

    @Override
    @Transactional
    public void addGoods(EGoods goods) {
        recoverLinkages(goods);
        goodsDAO.addGoods(goods);
        goods.getGoodsStoresByN().forEach(goodsStoreDAO::addLink);
        goods.getGoodsBarcodesByN().forEach(goodsBarcodeDAO::addBarcode);
    }

    @Override
    @Transactional
    public void removeGoods(int n) {
        EGoods goods = goodsDAO.getGoodsByN(n);
        goodsStoreDAO.removeAllLink(goods);
        goodsBarcodeDAO.removeAllBarcodes(goods);
        goodsDAO.removeGoods(n);
    }

    @Override
    @Transactional
    public void editGoods(EGoods goods) {
        recoverLinkages(goods);
        goodsDAO.editGoods(goods);
        goodsStoreDAO.removeAllLink(goods);
        goodsBarcodeDAO.removeAllBarcodes(goods);
        goods.getGoodsBarcodesByN().forEach(goodsBarcodeDAO::addBarcode);
        goods.getGoodsStoresByN().forEach(goodsStoreDAO::addLink);
    }

    @Override
    @Transactional
    public List<EGoods> goods() {
        List<EGoods> goods = goodsDAO.goods();
        //TODO for lazy loading
        for (EGoods g : goods) {
            Hibernate.initialize(g.getGoodsBarcodesByN());
            Hibernate.initialize(g.getGoodsStoresByN());
        }
        return goods;
    }

    @Override
    @Transactional
    public List<EStore> getStoresByGoods(EGoods goods) {
        return goods.getGoodsStoresByN().stream().map(EGoodsStores::getStoresByStoresN).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<EStore> getStoresByGoods(int n) {
        EGoods goods = goodsDAO.getGoodsByN(n);
        return goods.getGoodsStoresByN().stream().map(EGoodsStores::getStoresByStoresN).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void addStore(EGoods goods, EStore store) {
    }

    @Override
    @Transactional
    public void removeStore(EGoods goods, EStore store) {
        goodsStoreDAO.removeLink(goods, store);
    }
}
