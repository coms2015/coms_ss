package coms.server.service;

import coms.server.EditDenyException;
import coms.server.ReserveDeficiencyException;
import coms.server.dao.OrderDAO;
import coms.server.dao.StockDAO;
import coms.server.entity.EGoods;
import coms.server.entity.EOrder;
import coms.server.entity.EOrdersGoods;
import coms.server.entity.EStock;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDAO orderDAO;
    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private UserSession session;

    private void recoverLinkage(EOrder order) {
        if (order.getOrdersGoodsesByN() == null)
            order.setOrdersGoodsesByN(new ArrayList<>(0));
        order.getOrdersGoodsesByN().forEach(g -> g.setOrdersByOrdersN(order));
    }

    @Override
    @Transactional
    public void addOrder(EOrder order) {
        recoverLinkage(order);
        order.setUsersByUsersN(session.getUser());
        orderDAO.addOrder(order);
    }

    @Override
    @Transactional
    public void removeOrder(int n) {
        EOrder order = orderDAO.getOrderByN(n);
        orderDAO.removeOrder(n);
        for (EOrdersGoods ordersGoods : order.getOrdersGoodsesByN()) {
            EStock reserveStock = ordersGoods.getStockByStockN();
            if (reserveStock != null) {
                EStock newStock = new EStock();
                newStock.setAmount(reserveStock.getAmount());
                newStock.setGoodsByGoodsN(reserveStock.getGoodsByGoodsN());
                newStock.setStoresByStoresN(reserveStock.getStoresByStoresN());
                newStock.setPrice(reserveStock.getPrice());
                newStock.setInOrder(0);
                stockDAO.addStock(newStock);
                stockDAO.removeStock(reserveStock.getN());
            }
        }
    }

    @Override
    @Transactional
    public void getGoodsFromStock(int orderN) {
        EOrder order = orderDAO.getOrderByN(orderN);
        if (order.getExecuted() != 1)
            return;
        orderDAO.removeOrder(order.getN());
        for (EOrdersGoods goods : order.getOrdersGoodsesByN()) {
            EStock stock = goods.getStockByStockN();
            stockDAO.removeStock(stock.getN());
        }
    }

    @Override
    @Transactional
    public void reserveStock(int orderN) {
        EOrder order = orderDAO.getOrderByN(orderN);
        for (EOrdersGoods ordersGoods : order.getOrdersGoodsesByN()) {
            List<EStock> listStock = stockDAO.stockByCriteria(order.getStoresByStoresN(), ordersGoods.getGoodsByGoodsN(), ordersGoods.getAmount());
            if (listStock.size() == 0)
                throw new ReserveDeficiencyException();
            EStock stock = listStock.get(0);
            int oldAmount = stock.getAmount();
            stock.setAmount(ordersGoods.getAmount());
            stock.setInOrder(1);
            ordersGoods.setStockByStockN(stock);
            orderDAO.saveOrUpdateGoods(ordersGoods);
            stockDAO.editStock(stock);
            if (oldAmount > ordersGoods.getAmount()) {
                EStock newStock = new EStock();
                newStock.setStoresByStoresN(stock.getStoresByStoresN());
                newStock.setPrice(stock.getPrice());
                newStock.setGoodsByGoodsN(stock.getGoodsByGoodsN());
                newStock.setInOrder(0);
                newStock.setAmount(oldAmount - ordersGoods.getAmount());
                stockDAO.addStock(newStock);
            }
        }
        order.setCreatedAt(new Timestamp(new Date().getTime()));
        order.setExecuted(1);
        orderDAO.editOrder(order);
    }

    @Override
    @Transactional
    public void editOrder(EOrder order) throws EditDenyException {
        recoverLinkage(order);
        if (order.getExecuted() == 1)
            throw new EditDenyException();
        order.getOrdersGoodsesByN().forEach(orderDAO::saveOrUpdateGoods);
        orderDAO.editOrder(order);
    }

    @Override
    @Transactional
    public List<EOrder> orders() {
        List<EOrder> orders = orderDAO.orders();
        for (EOrder order : orders) {
            Hibernate.initialize(order);
            Hibernate.initialize(order.getStoresByStoresN());
            Collection<EOrdersGoods> goods = order.getOrdersGoodsesByN();
            Hibernate.initialize(goods);
            for (EOrdersGoods g : goods) {
                EGoods g1 = g.getGoodsByGoodsN();
                Hibernate.initialize(g1.getGoodsBarcodesByN());
                Hibernate.initialize(g1.getGoodsBarcodesByN());
            }
        }
        return orders;
    }

    @Transactional
    @Override
    public String orderAsString(ObjectMapper mapper) throws IOException {
        return mapper.writeValueAsString(orders());
    }

    @Override
    @Transactional
    public List<EOrder> ordersByUserN(int n) {
        List<EOrder> orders = orderDAO.ordersByUserN(n);
        for (EOrder order : orders) {
            Hibernate.initialize(order);
            Hibernate.initialize(order.getStoresByStoresN());
            Collection<EOrdersGoods> goods = order.getOrdersGoodsesByN();
            Hibernate.initialize(goods);
            for (EOrdersGoods g : goods) {
                EGoods g1 = g.getGoodsByGoodsN();
                Hibernate.initialize(g1.getGoodsBarcodesByN());
                Hibernate.initialize(g1.getGoodsBarcodesByN());
            }
        }
        return orders;
    }

    @Transactional
    @Override
    public String ordersByUserNAsString(int n, ObjectMapper mapper) throws IOException {
        return mapper.writeValueAsString(ordersByUserN(n));
    }
}
