package coms.server.service;

import coms.server.dao.UserDAO;
import coms.server.entity.ERole;
import coms.server.entity.EUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@SuppressWarnings("FieldCanBeLocal")
@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserSessionImpl implements UserSession,Serializable {
    private static long SESSION_IDLE_TIMEOUT = 300000; //5 min

    @Autowired
    private UserDAO userDAO;

    private EUser user;
    private long lastActiveTime;
    private List<String> modules;

    @Override
    public boolean isLoggedIn() {
        if (user != null) {
            long now = new Date().getTime();
            if (now - lastActiveTime < SESSION_IDLE_TIMEOUT) {
                lastActiveTime = now;
                return true;
            } else {
                user = null;
            }
        }
        return false;
    }

    @Override
    public boolean tryToAuth(String login, String hash) {
        if ((user = userDAO.authUser(login, hash)) != null) {
            lastActiveTime = new Date().getTime();
            modules = Arrays.asList(user.getRolesByRolesN().getModules().split(","));
            return true;
        }
        else
            return false;
    }

    @Override
    public void logOut() {
        user = null;
    }

    @Override
    public ERole getRole() {
        if (user != null)
            return user.getRolesByRolesN();
        return null;
    }

    @Override
    public List<String> getModules() {
        return modules;
    }

    @Override
    public boolean checkModuleAccess(String module) {
        return modules.contains(module);
    }

    @Override
    public EUser getUser() {
        return user;
    }
}