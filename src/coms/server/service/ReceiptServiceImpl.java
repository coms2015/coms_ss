package coms.server.service;

import coms.server.dao.ReceiptDAO;
import coms.server.dao.StockDAO;
import coms.server.entity.EGoods;
import coms.server.entity.EReceipt;
import coms.server.entity.EReceiptsGoods;
import coms.server.entity.EStock;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class ReceiptServiceImpl implements ReceiptService {
    @Autowired
    private ReceiptDAO receiptDAO;
    @Autowired
    private StockDAO stockDAO;

    private void recoverLinkage(EReceipt receipt) {
        if (receipt.getReceiptsGoodsesByN() == null)
            receipt.setReceiptsGoodsesByN(new ArrayList<>(0));
        receipt.getReceiptsGoodsesByN().forEach(g -> g.setReceiptsByReceiptsN(receipt));
    }

    @Override
    @Transactional
    public void addReceipt(EReceipt receipt) {
        recoverLinkage(receipt);
        receiptDAO.addReceipt(receipt);
    }

    @Override
    @Transactional
    public void removeReceipt(int n) {
        receiptDAO.removeReceipt(n);
    }

    @Override
    @Transactional
    public void putReceiptToStock(int receiptN) {
        EReceipt receipt = receiptDAO.getReceiptByN(receiptN);
        for (EReceiptsGoods goods : receipt.getReceiptsGoodsesByN()) {
            EStock stock = new EStock();
            stock.setAmount(goods.getAmount());
            stock.setGoodsByGoodsN(goods.getGoodsByGoodsN());
            stock.setStoresByStoresN(receipt.getStoresByStoresN());
            stock.setInOrder(0);
            //TODO если останется время, добавить цену в состав прихода
            stock.setPrice(BigDecimal.valueOf(0));
            stockDAO.addStock(stock);
        }
        receiptDAO.removeReceipt(receipt.getN());
    }

    @Override
    @Transactional
    public void editReceipt(EReceipt receipt) {
        recoverLinkage(receipt);
        receiptDAO.editReceipt(receipt);
    }

    @Override
    @Transactional
    public List<EReceipt> receipts() {
        List<EReceipt> receipts = receiptDAO.receipts();
        for (EReceipt receipt : receipts) {
            Collection<EReceiptsGoods> goods = receipt.getReceiptsGoodsesByN();
            for (EReceiptsGoods g : goods) {
                EGoods g1 = g.getGoodsByGoodsN();
                Hibernate.initialize(g1.getGoodsBarcodesByN());
                Hibernate.initialize(g1.getGoodsStoresByN());
            }
        }
        return receipts;
    }
}
