package coms.server.service;

import coms.server.entity.EStock;
import coms.server.entity.EStore;

import java.util.List;

public interface MoveService {
    void moveStock(List<EStock> stock, EStore toStore);
}
