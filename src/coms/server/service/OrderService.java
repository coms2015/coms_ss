package coms.server.service;

import coms.server.EditDenyException;
import coms.server.ReserveDeficiencyException;
import coms.server.entity.EOrder;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.List;

public interface OrderService {
    void addOrder(EOrder order);

    void removeOrder(int n);

    void getGoodsFromStock(int orderN);

    void reserveStock(int orderN) throws ReserveDeficiencyException;

    void editOrder(EOrder order) throws EditDenyException;

    List<EOrder> orders();

    String orderAsString(ObjectMapper mapper) throws IOException;

    List<EOrder> ordersByUserN(int n);

    String ordersByUserNAsString(int n, ObjectMapper mapper) throws IOException;
}
