package coms.server.service;

import coms.server.entity.EStore;

import java.util.List;

public interface StoreService {
    void addStore(EStore store);
    void removeStore(Integer n);
    void editStore(EStore store);
    List<EStore> stores();
}
