package coms.server.service;

import coms.server.dao.StockDAO;
import coms.server.entity.EStock;
import coms.server.entity.EStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StockServiceImpl implements StockService {
    @Autowired
    private StockDAO stockDAO;

    @Override
    @Transactional
    public List<EStock> getStock() {
        return stockDAO.stock(null, false);
    }

    @Override
    @Transactional
    public List<EStock> getStockUnreserved() {
        return stockDAO.stock(null, true);
    }

    @Override
    @Transactional
    public List<EStock> getStockByStore(EStore store) {
        return stockDAO.stock(store, false);
    }

    @Override
    @Transactional
    public List<EStock> getStockByStoreUnreserved(EStore store) {
        return stockDAO.stock(store, true);
    }
}
