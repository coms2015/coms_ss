package coms.server.service;

import coms.server.entity.EGoods;
import coms.server.entity.EStore;

import java.util.List;

public interface GoodsService {
    void addGoods(EGoods goods);

    void removeGoods(int n);

    void editGoods(EGoods goods);

    List<EGoods> goods();

    List<EStore> getStoresByGoods(EGoods goods);

    List<EStore> getStoresByGoods(int n);

    void addStore(EGoods goods, EStore store);

    void removeStore(EGoods goods, EStore store);
}
