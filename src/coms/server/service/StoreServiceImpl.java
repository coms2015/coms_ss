package coms.server.service;

import coms.server.dao.StoreDAO;
import coms.server.entity.EStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private StoreDAO storeDAO;

    @Override
    @Transactional
    public void addStore(EStore store) {
        storeDAO.addStore(store);
    }

    @Override
    @Transactional
    public void removeStore(Integer n) {
        storeDAO.removeStore(n);
    }

    @Override
    @Transactional
    public void editStore(EStore store) {
        storeDAO.editStore(store);
    }

    @Override
    @Transactional
    public List<EStore> stores(){
        return storeDAO.stores();
    }
}
