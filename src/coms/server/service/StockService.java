package coms.server.service;

import coms.server.entity.EStock;
import coms.server.entity.EStore;

import java.util.List;

public interface StockService {
    List<EStock> getStock();

    List<EStock> getStockUnreserved();

    List<EStock> getStockByStore(EStore store);

    List<EStock> getStockByStoreUnreserved(EStore store);
}
