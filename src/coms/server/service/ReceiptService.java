package coms.server.service;

import coms.server.entity.EOrder;
import coms.server.entity.EReceipt;

import java.util.List;

public interface ReceiptService {
    void addReceipt(EReceipt receipt);

    void removeReceipt(int n);

    void putReceiptToStock(int receiptN);

    void editReceipt(EReceipt receipt);

    List<EReceipt> receipts();
}
