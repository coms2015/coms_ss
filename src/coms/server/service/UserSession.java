package coms.server.service;

import coms.server.entity.ERole;
import coms.server.entity.EUser;

import java.util.List;

public interface UserSession {
    boolean isLoggedIn();
    boolean tryToAuth(String login, String hash);
    void logOut();
    ERole getRole();

    List<String> getModules();

    boolean checkModuleAccess(String module);
    EUser getUser();
}
