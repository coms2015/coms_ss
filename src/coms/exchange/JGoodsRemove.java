package coms.exchange;

public class JGoodsRemove {
    private Integer goodsRemove;
    private Integer goodsN;

    public Integer getGoodsRemove() {
        return goodsRemove;
    }

    public void setGoodsRemove(Integer goodsRemove) {
        this.goodsRemove = goodsRemove;
    }

    public Integer getGoodsN() {
        return goodsN;
    }

    public void setGoodsN(Integer goodsN) {
        this.goodsN = goodsN;
    }
}
