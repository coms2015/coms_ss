package coms.exchange;


import coms.server.entity.EOrder;

public class JOrderEdit {
    private Integer editOrder;
    private EOrder order;

    public Integer getEditOrder() {
        return editOrder;
    }

    public void setEditOrder(Integer editOrder) {
        this.editOrder = editOrder;
    }

    public EOrder getOrder() {
        return order;
    }

    public void setOrder(EOrder order) {
        this.order = order;
    }
}
