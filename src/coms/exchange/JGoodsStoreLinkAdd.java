package coms.exchange;

import coms.server.entity.EGoods;
import coms.server.entity.EStore;

public class JGoodsStoreLinkAdd {
    private Integer addLinkGoodsStore;
    private EGoods goods;
    private EStore store;

    public Integer getAddLinkGoodsStore() {
        return addLinkGoodsStore;
    }

    public void setAddLinkGoodsStore(Integer addLinkGoodsStore) {
        this.addLinkGoodsStore = addLinkGoodsStore;
    }

    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }

    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }
}
