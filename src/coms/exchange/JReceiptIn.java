package coms.exchange;

import coms.server.entity.EReceipt;

public class JReceiptIn {
    private Integer inReceipt;
    private EReceipt receipt;

    public Integer getInReceipt() {
        return inReceipt;
    }

    public void setInReceipt(Integer inReceipt) {
        this.inReceipt = inReceipt;
    }

    public EReceipt getReceipt() {
        return receipt;
    }

    public void setReceipt(EReceipt receipt) {
        this.receipt = receipt;
    }
}
