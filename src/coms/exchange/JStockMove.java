package coms.exchange;

import coms.server.entity.EStock;
import coms.server.entity.EStore;

import java.util.List;

public class JStockMove {
    private Integer stockMove;
    private List<EStock> stock;
    private EStore store;

    public Integer getStockMove() {
        return stockMove;
    }

    public void setStockMove(Integer stockMove) {
        this.stockMove = stockMove;
    }

    public List<EStock> getStock() {
        return stock;
    }

    public void setStock(List<EStock> stock) {
        this.stock = stock;
    }

    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }
}
