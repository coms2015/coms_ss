package coms.exchange;

import coms.server.entity.EGoods;

public class JGoodsEdit {
    private Integer editGoods;
    private EGoods goods;

    public Integer getEditGoods() {
        return editGoods;
    }

    public void setEditGoods(Integer editGoods) {
        this.editGoods = editGoods;
    }

    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }
}