package coms.exchange;

import coms.server.entity.EStore;

public class JStoreEdit {
    private Integer editStore;
    private EStore store;

    public Integer getEditStore() {
        return editStore;
    }

    public void setEditStore(Integer editStore) {
        this.editStore = editStore;
    }

    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }
}
