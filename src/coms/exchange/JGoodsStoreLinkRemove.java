package coms.exchange;

import coms.server.entity.EGoods;
import coms.server.entity.EStore;

public class JGoodsStoreLinkRemove {
    private Integer removeLinkGoodsStore;
    private EGoods goods;
    private EStore store;

    public Integer getRemoveLinkGoodsStore() {
        return removeLinkGoodsStore;
    }

    public void setRemoveLinkGoodsStore(Integer removeLinkGoodsStore) {
        this.removeLinkGoodsStore = removeLinkGoodsStore;
    }

    public EGoods getGoods() {
        return goods;
    }

    public void setGoods(EGoods goods) {
        this.goods = goods;
    }

    public EStore getStore() {
        return store;
    }

    public void setStore(EStore store) {
        this.store = store;
    }
}
