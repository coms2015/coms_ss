package coms.exchange;

public class JStoreRemove {
    private Integer storeRemove;
    private Integer storeN;

    public Integer getStoreRemove() {
        return storeRemove;
    }

    public void setStoreRemove(Integer storeRemove) {
        this.storeRemove = storeRemove;
    }

    public Integer getStoreN() {
        return storeN;
    }

    public void setStoreN(Integer storeN) {
        this.storeN = storeN;
    }
}
