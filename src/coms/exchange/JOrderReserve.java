package coms.exchange;

import coms.server.entity.EOrder;

public class JOrderReserve {
    private Integer reserveOrder;
    private EOrder order;

    public Integer getReserveOrder() {
        return reserveOrder;
    }

    public void setReserveOrder(Integer reserveOrder) {
        this.reserveOrder = reserveOrder;
    }

    public EOrder getOrder() {
        return order;
    }

    public void setOrder(EOrder order) {
        this.order = order;
    }
}
