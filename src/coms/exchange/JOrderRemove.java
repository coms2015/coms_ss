package coms.exchange;

import coms.server.entity.EOrder;

public class JOrderRemove {
    private Integer removeOrder;
    private EOrder order;

    public Integer getRemoveOrder() {
        return removeOrder;
    }

    public void setRemoveOrder(Integer removeOrder) {
        this.removeOrder = removeOrder;
    }

    public EOrder getOrder() {
        return order;
    }

    public void setOrder(EOrder order) {
        this.order = order;
    }
}
