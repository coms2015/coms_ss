package coms.exchange;

import coms.server.entity.EReceipt;

public class JReceiptAdd {
    private Integer addReceipt;
    private EReceipt receipt;

    public Integer getAddReceipt() {
        return addReceipt;
    }

    public void setAddReceipt(Integer addReceipt) {
        this.addReceipt = addReceipt;
    }

    public EReceipt getReceipt() {
        return receipt;
    }

    public void setReceipt(EReceipt receipt) {
        this.receipt = receipt;
    }
}
