package coms.exchange;

import coms.server.entity.EReceipt;

public class JReceiptRemove {
    private Integer removeReceipt;
    private EReceipt receipt;

    public Integer getRemoveReceipt() {
        return removeReceipt;
    }

    public void setRemoveReceipt(Integer removeReceipt) {
        this.removeReceipt = removeReceipt;
    }

    public EReceipt getReceipt() {
        return receipt;
    }

    public void setReceipt(EReceipt receipt) {
        this.receipt = receipt;
    }
}
